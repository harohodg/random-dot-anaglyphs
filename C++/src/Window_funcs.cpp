/*
 * Window_funcs.cpp
 *
 *  Created on: 2016-12-04
 *      Author: harold Hodgins
 */
#include "Window_funcs.hpp"
#include "Anaglyph_funcs.hpp"

static GLdouble zoomFactor = 1.0;
static GLint height;

extern int winWidth, winHeight;
extern GLubyte anaglyph[imageHeight][imageWidth][3];

void display(void) {
	glClear(GL_COLOR_BUFFER_BIT);
	glRasterPos2i(0, 0);
	glDrawPixels(winWidth, winHeight, GL_RGB, GL_UNSIGNED_BYTE,
			anaglyph);
	glFlush();
}

void reshape(int w, int h) {
	glViewport(0, 0, (GLsizei) w, (GLsizei) h);
	height = (GLint) h;
	winWidth = w;
	winHeight= h;
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0.0, (GLdouble) w, 0.0, (GLdouble) h);
	//gluOrtho2D(0, w, h, 0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void motion(int x, int y) {
	static GLint screeny;

	screeny = height - (GLint) y;
	glRasterPos2i(x, screeny);
	glPixelZoom(zoomFactor, zoomFactor);
	glCopyPixels(0, 0, winWidth, winHeight, GL_COLOR);
	glPixelZoom(1.0, 1.0);
	glFlush();
}

void keyboard(unsigned char key, int x, int y) {
	switch (key) {
	case 'r':
	case 'R':
		zoomFactor = 1.0;
		glutPostRedisplay();
		printf("zoomFactor reset to 1.0\n");

		break;
	case 'z':
		zoomFactor += 0.5;
		if (zoomFactor >= 3.0)
			zoomFactor = 3.0;
		printf("zoomFactor is now %4.1f\n", zoomFactor);
		break;
	case 'Z':
		zoomFactor -= 0.5;
		if (zoomFactor <= 0.5)
			zoomFactor = 0.5;
		printf("zoomFactor is now %4.1f\n", zoomFactor);
		break;
	case 27:
		exit(0);
		break;
	default:
		break;
	}
}



