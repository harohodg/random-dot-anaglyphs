/*
 * Window_funcs.hpp
 *
 *  Created on: 2016-12-04
 *      Author: Harold Hodgins
 */

#ifndef WINDOW_FUNCS_HPP_
#define WINDOW_FUNCS_HPP_

#include <GL/glut.h>
#include <stdlib.h>
#include <stdio.h>

void display(void);
void reshape(int w, int h);
void motion(int x, int y);
void keyboard(unsigned char key, int x, int y);

#endif /* WINDOW_FUNCS_HPP_ */
