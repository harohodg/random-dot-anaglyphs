/*
 * Anaglyph_funcs.cpp
 *
 *  Created on: 2016-12-04
 *      Author: Harold Hodgins
 */

#include "Anaglyph_funcs.hpp"
#include "Image_funcs.hpp"

extern int winWidth, winHeight;

GLubyte anaglyph[imageHeight][imageWidth][3];
bool leftPattern[imageHeight][imageWidth];
bool rightPattern[imageHeight][imageWidth];
GLubyte depthMap[imageHeight][imageWidth];

bool animateImage = true;
int boxLeftEdge = BOX_X_START;
int boxBottomEdge = BOX_Y_START;
int depth = 255;

int hSpeed = 10;
int vSpeed = 10;

int minShift = 0;
int maxShift = 30;

void printDepthMap()
{
	for (int row = imageHeight - 1; row >= 0; row--)
	{
		printf("Row %d : ", row);
		for (int col = 0; col < imageWidth; col++)
		{
			printf(" %3d", depthMap[row][col]);

		}
		printf("\n");
	}
}

void shiftDepthMap(int minShift, int maxShift)
{
	float scale = (255 / maxShift);
	int shift;
	//Shift a small portion to the right create the right eye pattern
	for (int i = 0; i < imageHeight; i++)
	{
		for (int j = 0; j < imageWidth; j++)
		{
			shift = int(depthMap[i][j] / scale + minShift);
			shift = (j - shift > 0) ? shift : 0;
			rightPattern[i][j] = leftPattern[i][j - shift];
		}
	}

}

void setRandomPattern(void)
{
	int i, j, c;
	//Create left eye random pattern
	for (i = 0; i < imageHeight; i++)
	{
		for (j = 0; j < imageWidth; j++)
		{
			//c = ((((i&0x8)==0)^((j&0x8))==0))*255;
			c = (rand() % 2);
			leftPattern[i][j] = c;
		}
	}
}

/*
 Function to right shift the background to make it more random
 */
void shiftBackground(int shift)
{
	for (int i = 0; i < imageHeight; i++)
	{
		for (int j = shift; j < imageWidth; j++)
		{
			rightPattern[i][j] = leftPattern[i][j - shift];
		}
	}
}
void shiftSquare(int xStart, int yStart, int boxSize, int shift)
{
	int i, j, localShift;

	//Shift a small portion to the right create the right eye pattern
	for (i = xStart; i < xStart + boxSize; i++)
	{
		for (j = yStart; j < yStart + boxSize; j++)
		{
			localShift = shift;
			localShift = (j - localShift > 0) ? localShift : 0;
			localShift = (j - localShift < imageWidth) ? localShift : imageWidth;
			rightPattern[i][j] = leftPattern[i][j - localShift];
		}
	}
}

void mergePatterns()
{
	int i, j;
	//Color final image
	for (i = 0; i < imageHeight; i++)
	{
		for (j = 0; j < imageWidth; j++)
		{
			anaglyph[i][j][0] = rightPattern[i][j] * 255; //Red
			anaglyph[i][j][1] = leftPattern[i][j] * 255; //Blue
			anaglyph[i][j][2] = leftPattern[i][j] * 255; //Green
		}
	}
}

/*
 Function to create the actual Anaglyph
 */
void makeRandomDotAnaglyph(void)
{

	//setRandomPattern();
	memcpy(rightPattern, leftPattern, imageHeight * imageWidth * sizeof(bool));
	shiftDepthMap(minShift, maxShift);
	mergePatterns();
}

void setDepthMap(char* pattern, unsigned char value)
{
	if (strcmp(pattern, "box") == 0)
	{
		for (int i = BOX_Y_START; i < BOX_Y_START + BOX_SIZE; i++)
		{
			for (int j = BOX_X_START; j < BOX_X_START + BOX_SIZE; j++)
			{
				depthMap[i][j] = value;
			}
		}

	}
	else
	{
		loadImage(pattern);
	}
}

void setBoxMap(int xStart, int yStart, int size, int depth)
{
	for (int i = yStart; i < yStart + size; i++)
	{
		for (int j = xStart; j < xStart + size; j++)
		{
			depthMap[i][j] = depth;
		}
	}
}

void clearDepthMap()
{
	for (int i = 0; i < imageHeight; i++)
	{
		for (int j = 0; j < imageWidth; j++)
		{
			depthMap[i][j] = 0;
		}
	}
}

bool depthMapInRow(int row)
{
	bool result = false;
	int col = 0;
	while (col < imageWidth && result == false)
	{
		result = result || depthMap[row][col] != 0;
		col++;
	}
	return result;
}

bool depthMapInCol(int col)
{
	bool result = false;
	int row = 0;
	while (row < imageHeight && result == false)
	{
		result = result || depthMap[row][col] != 0;
		row++;
	}
	return result;
}

bool shiftRight(int hShift)
{
	if (depthMapInCol(imageWidth - 1) == false)
	{
		for (int row = 0; row < imageHeight; row++)
		{
			for (int col = imageWidth - 1; col > hShift - 1; col--)
			{
				depthMap[row][col] = depthMap[row][col - hShift];
			}
			depthMap[row][0] = 0;

		}
		return true;
	}
	else
	{
		return false;
	}
}

bool shiftLeft(int hShift)
{
	if (depthMapInCol(0) == false)
	{
		for (int row = 0; row < imageHeight; row++)
		{
			for (int col = 0; col < imageWidth - hShift; col++)
			{
				depthMap[row][col] = depthMap[row][col + hShift];
			}
			depthMap[row][imageWidth - 1] = 0;

		}
		return true;
	}
	else
	{
		return false;
	}
}

bool shiftUp(int vShift)
{
	if (depthMapInRow(imageHeight - 1) == false)
	{
		for (int col = 0; col < imageWidth; col++)
		{
			for (int row = imageHeight - 1; row > vShift - 1; row--)
			{
				//printf("Going Up : Row %d -> %d \n",row-vShift,row);
				depthMap[row][col] = depthMap[row - vShift][col];
			}
			depthMap[0][col] = 0;
		}
		return true;
	}
	else
	{
		return false;
	}
}

bool shiftDown(int vShift)
{
	if (depthMapInRow(0) == false)
	{
		for (int col = 0; col < imageWidth; col++)
		{

			for (int row = 0; row < imageHeight - vShift; row++)
			{
				//printf("Going Down : Row %d -> %d \n",row+vShift,row);
				depthMap[row][col] = depthMap[row + vShift][col];
			}
			depthMap[imageHeight - 1][col] = 0;

		}
		return true;
	}
	else
	{
		return false;
	}
}

struct Tuple
{
	bool a;
	bool b;
};
/*
 Function to move the actual depthmap image around
 */
Tuple moveDepthMap(int hShift, int vShift)
{
	bool hShifted = true, vShifted = true;
	int i = 0;
	if (hShift > 0) //Shifting right
	{
		while (i < hShift && hShifted == true)
		{
			hShifted = shiftRight(1);
			i += 1;
		}

	}
	else if (hShift < 0) //Shifting left
	{
		while (i < -1 * hShift && hShifted == true)
		{
			hShifted = shiftLeft(1);
			i += 1;
		}

	}
	i = 0;
	if (vShift > 0) //Shifting up
	{
		while (i < vShift && vShifted == true)
		{
			vShifted = shiftUp(1);
			i += 1;
		}
	}
	else if (vShift < 0) //shifting down
	{
		while (i < -1*vShift && vShifted == true)
		{
			vShifted = shiftDown(1);
			i += 1;
		}
	}
	Tuple r =
	{ hShifted, vShifted };
	return r;
}

void animateAnaglyph(int x)
{

	Tuple shifts = moveDepthMap(hSpeed, vSpeed);
	if (shifts.a == false)
	{
		hSpeed *= -1;
	}
	if (shifts.b == false)
	{
		vSpeed *= -1;
	}

	makeRandomDotAnaglyph();
	glutPostRedisplay();

	if (animateImage == true)
	{
		glutTimerFunc(50, animateAnaglyph, 1);
	}
}
