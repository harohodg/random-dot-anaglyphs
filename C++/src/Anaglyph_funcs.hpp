/*
 * Anaglyph_funcs.hpp
 *
 *  Created on: 2016-12-04
 *      Author: Harold Hodgins
 */

#ifndef ANAGLYPH_FUNCS_HPP_
#define ANAGLYPH_FUNCS_HPP_

#include <GL/glut.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define	 imageWidth 1000
#define	 imageHeight 1000

#define BOX_SIZE 300
#define BOX_X_START 2
#define BOX_Y_START 2


#define BACKGROUND_SHIFT 5
#define MIN_SHIFT 7
#define MAX_SHIFT 20

void makeRandomDotAnaglyph(void);
void setDepthMap(char* shape,unsigned char value);
void animateAnaglyph(int x);
void setRandomPattern();

#endif /* ANAGLYPH_FUNCS_HPP_ */
