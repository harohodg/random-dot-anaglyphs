/*
 * Image_funcs.cpp
 *
 *  Created on: 2016-12-04
 *      Author: Harold Hodgins
 */

#include "Image_funcs.hpp"


extern GLubyte anaglyph[imageHeight][imageWidth][3];
extern GLubyte depthMap[imageHeight][imageWidth];
extern int winWidth, winHeight;

void saveImage(Mat imageData, char* name) {
	vector<int> compression_params;
	compression_params.push_back(CV_IMWRITE_PNG_COMPRESSION);
	compression_params.push_back(9);
	try {
		imwrite(name, imageData, compression_params);
	} catch (const exception& e) {
		printf("Exception converting image to PNG format: %s\n", e.what());
	}
}

void exportAnaglyph( char* name) {
	Mat outImage(imageHeight, imageWidth,CV_8UC3);

	for (int i = 0; i < outImage.rows; i++) {
		for (int j = 0; j < outImage.cols; j++) {
			Vec3b& bgr = outImage.at<Vec3b>(i, j);
			bgr[0] = anaglyph[i][j][2]; // Blue
			bgr[1] = anaglyph[i][j][1];
			bgr[2] = anaglyph[i][j][0];
		}
	}
	flip(outImage, outImage, 0);

	saveImage(outImage, name);
}

void loadImage(char * name) {
	Scalar intensity;
	Mat image = imread(name, CV_LOAD_IMAGE_GRAYSCALE);

	if (!image.data) {
		printf("No image data \n");
	}
	flip(image, image, 0);

	for (int i = 0; i < min(image.rows,winHeight); i++) {
		for (int j = 0; j < min(image.cols,winWidth); j++) {
			intensity = image.at<uchar>(i, j);
			depthMap[i][j] = intensity.val[0];
		}
	}
}

