/*
 * Image_funcs.hpp
 *
 *  Created on: 2016-12-04
 *      Author: Harold Hodgins
 */

#ifndef IMAGE_FUNCS_HPP_
#define IMAGE_FUNCS_HPP_

#include <vector>
#include <stdio.h>
using namespace std;

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>
using namespace cv;

#include "Anaglyph_funcs.hpp"

void loadImage(char * name);
void exportAnaglyph( char* name);

#endif /* IMAGE_FUNCS_HPP_ */
